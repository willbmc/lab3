package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashCardSetOfSets
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashcardSetDetailActivity : AppCompatActivity() {
    private var sets = Flashcard.Companion.getHardcodedFlashcards().toMutableList()
    private lateinit var setRecyclerView: RecyclerView
    private var apapter = FlashCardSetOfSets(sets)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_flash_card_set_veiw)

        LinearLayoutManager(this)
        val itemAdapter1 = FlashCardSetOfSets(sets)

        val recyclerView =findViewById<RecyclerView>(R.id.RecyclerLineirVeiw)
        recyclerView.adapter = itemAdapter1

        val textView: TextView = findViewById(R.id.button3)
        textView.setOnClickListener{addSetOfSets()}
    }

    fun addSetOfSets() {

        sets.add(Flashcard("New term", "New Def"))
        val itemAdapter1 = FlashCardSetOfSets(sets)

        val recyclerView =findViewById<RecyclerView>(R.id.RecyclerLineirVeiw)
        recyclerView.adapter = itemAdapter1
    }
}