package cs.mad.flashcards.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.content.Context
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.adapters.FlashCardSetOfSets
import cs.mad.flashcards.adapters.FlashcardSetAdapter
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet
import cs.mad.flashcards.entities.FlashcardSet.Companion.getHardcodedFlashcardSets


/*
===================================================================================================================

     Reference documentation for recyclers: https://developer.android.com/guide/topics/ui/layout/recyclerview

===================================================================================================================
 */

class MainActivity : AppCompatActivity() {
    private var sets = FlashcardSet.Companion.getHardcodedFlashcardSets().toMutableList()
    private lateinit var setRecyclerView: RecyclerView
    private var apapter = FlashcardSetAdapter(sets)



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        GridLayoutManager(this,2)

        val itemAdapter = FlashcardSetAdapter(sets)

        val recyclerView =findViewById<RecyclerView>(R.id.recyler_grid_veiw)

        recyclerView.adapter = itemAdapter

        val textView: TextView = findViewById(R.id.button4)
        textView.setOnClickListener{addSetOfSets()}



        /*
            connect to views using findViewById
            setup views here - recyclerview, button
            don't forget to notify the adapter if the data set is changed
         */
    }

     fun addSetOfSets() {

        sets.add(FlashcardSet("New term"))
        val itemAdapter = FlashcardSetAdapter(sets)
        val recyclerView =findViewById<RecyclerView>(R.id.recyler_grid_veiw)
        recyclerView.adapter = itemAdapter
    }
}