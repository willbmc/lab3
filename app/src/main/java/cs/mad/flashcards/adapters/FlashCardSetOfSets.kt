package cs.mad.flashcards.adapters

import android.annotation.SuppressLint
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import cs.mad.flashcards.R
import cs.mad.flashcards.activities.FlashcardSetDetailActivity
import cs.mad.flashcards.entities.Flashcard
import cs.mad.flashcards.entities.FlashcardSet

class FlashCardSetOfSets(val s: MutableList<Flashcard>):
    RecyclerView.Adapter<FlashCardSetOfSets.ViewHolder1>()
    {
        private var sets = s


        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        class ViewHolder1(view: View) : RecyclerView.ViewHolder(view) {
            val itemText : TextView = view.findViewById(R.id.flashcardsets)

        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder1 {
            /*
                this is written for you but understand what is happening here
                the layout for an individual item is being inflated
                the inflated layout is passed to view holder for storage

                THE ITEM LAYOUT STILL NEEDS TO BE SETUP IN THE LAYOUT EDITOR
             */
            return ViewHolder1(LayoutInflater.from(viewGroup.context).inflate(R.layout.item_flashcardsetofset, viewGroup, false))
        }

        // Replace the contents of a view (invoked by the layout manager)

        override fun onBindViewHolder(viewHolder1: ViewHolder1, position: Int) {
            /*
                fill the contents of the view using references in view holder and current position in data set

                to launch FlashcardSetDetailActivity ->
                viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))
             */
            viewHolder1.itemText.text =  sets[position].term
//            viewHolder.itemView.setOnClickListener{viewHolder.itemView.context.startActivity(Intent(viewHolder.itemView.context, FlashcardSetDetailActivity::class.java))}

        }




        override fun getItemCount(): Int {
            // return the size of the data set
            return sets.size
        }
    }